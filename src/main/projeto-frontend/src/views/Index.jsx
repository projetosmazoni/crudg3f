import React from "react";

import PageHeader from "components/PageHeader/PageHeader.jsx";
import Login from "./login/login";

class Index extends React.Component {
  componentDidMount() {
    document.body.classList.toggle("index-page");
  }
  componentWillUnmount() {
    document.body.classList.toggle("index-page");
  }
  render() {
    return (
      <>
        <div className="wrapper">
          <PageHeader />
          <div className="main">
            <Login />
          </div>
        </div>
      </>
    );
  }
}

export default Index;
