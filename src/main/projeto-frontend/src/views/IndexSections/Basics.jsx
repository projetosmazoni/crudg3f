import React from "react";

import {Col, Container, Row} from "reactstrap";
import { Link } from "react-router-dom";

import axios from 'axios';
const API_MEMBRO_URL = 'http://localhost:8888/api/cliente/';

class Basics extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      inputFocus: false,
      clientes: []
    };

    this.buscarMembros();
  }

  buscarMembros = () => {
    axios.get(API_MEMBRO_URL).then(res => {
      this.setState({ clientes: res.data });
    });
  };

  render() {
    return (
        <Container>
          <Row>
            <Link to="new-user" rel="tooltip" className="btn btn-primary">Novo Cliente</Link>
          </Row>
          <Row>
            <Col lg="12" sm="12">
              <h3>Clientes Cadastrados</h3>
              <table className="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Cpf</th>
                    <th className="text-right">Ação</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    this.state.clientes.length ?
                        this.state.clientes.map((val, index) => {
                        return (
                          <tr>
                            <th>{index + 1}</th>
                            <td>{val.nome}</td>
                            <td>{val.cpf}</td>
                            <td className="td-actions text-right">
                              <Link to={'user-details/' + val.id} rel="tooltip" className="btn btn-info btn-simple btn-icon btn-sm mr-2">
                                <i className="tim-icons icon-single-02"></i>
                              </Link>
                            </td>
                          </tr>
                        )
                      })
                    : null
                  }
                </tbody>
              </table>
            </Col>
          </Row>
        </Container>
    );
  }
}

export default Basics;
