import React from "react";

import {Col, Container, Row} from "reactstrap";

import  { Redirect } from 'react-router-dom'
import axios from 'axios';
import Swal from 'sweetalert2'

const API_MEMBRO_URL = 'http://localhost:8888/api/login/';

class Login extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      formControls: {
        nome: { value : ''},
        senha: { value : ''},
      }
    }
  }

  changeHandler = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      formControls: {
        ...this.state.formControls, [name]: { ...this.state.formControls[name], value }
      }
    })
  }

  logar = () => {
    let objeto = {
      nome:  this.state.formControls.nome.value,
      senha: this.state.formControls.senha.value,
    }

    axios.post(`${API_MEMBRO_URL}`, objeto).then(res => {
      Swal.fire('Logado com Sucesso', 'success');
      window.location.href = "http://localhost:3000/dashboard";
    }, (e) => {
      Swal.fire('Login invalidao', 'error');
    });
  }

  render() {
    return (
        <Container>
          <form>
            <Row>
              <Col lg="12" sm="12">
                <h3>Login</h3>
                <div className="form-group">
                  <label>Nome</label>
                  <input type="text"
                         name="nome"
                         className="form-control"
                         value={this.state.formControls.nome.value}
                         onChange={this.changeHandler}
                  />
                </div>

                <div className="form-group">
                  <label>Senha</label>
                  <input type="password"
                         name="senha"
                         className="form-control"
                         value={this.state.formControls.senha.value}
                         onChange={this.changeHandler}/>
                </div>
              </Col>
            </Row>

            <Row>
              <Col lg="12" sm="12">
                <button type="button" onClick={this.logar} className="pull-right btn btn-success">Logar</button>
              </Col>
            </Row>
          </form>
        </Container>
    );
  }
}

export default Login;
