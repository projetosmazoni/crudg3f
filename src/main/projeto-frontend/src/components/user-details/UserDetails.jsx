import React from "react";
import axios from "axios";
import {Col, Container, Row} from "reactstrap";
import {Link} from "react-router-dom";
import Swal from 'sweetalert2'
import InputMask from 'react-input-mask';

const API_MEMBRO_URL = 'http://localhost:8888/api/cliente/';
const API_CEP_URL = 'http://localhost:8888/api/cep';

const maginTop = {
  marginTop: '-5px'
}

class UserDetails extends React.Component {

  constructor(props) {
    super(props);

    const id = this.props.match.params.id;

    this.state = {
      formControls: {
        id: { value : ''},
        nome: { value : ''},
        cpf: { value : ''},
      },
      enderecos: [{
        id: { value : ''},
        cep: { value : ''},
        logradouro: { value : ''},
        bairro: { value : ''},
        cidade: { value : ''},
        uf: { value : ''},
        complemento: { value : ''},
      }],
      telefones: [{
        id: { value : '' },
        tipo: { value : '' },
        numero: { value : '' },
      }],
      emails: [ {
        id: { value : ''},
        descricao: { value : ''}
      }]
    };

    if (id) {
      this.buscarMembrosPorId(id);
    }
  }

  buscarMembrosPorId = (id) => {
    axios.get(`${API_MEMBRO_URL}/${id}`).then(res => {
      let membro = res.data;
      this.populaStateValoresAtualizadosNoBanco(membro);
    });
  };

  carregaDadosViaCep = (cep, index) => {
    if (cep != null && cep != "") {
      axios.get(`${API_CEP_URL}/${cep}`).then(res => {
        const data = res.data;
        this.state.enderecos[index].cep = data.cep;
        this.state.enderecos[index].bairro = data.bairro;
        this.state.enderecos[index].cidade = data.localidade;
        this.state.enderecos[index].complemento = data.cep;
        this.state.enderecos[index].logradouro = data.logradouro;
        this.state.enderecos[index].uf = data.uf;
        this.setState(this.state);
      })
    }
  }

  populaStateValoresAtualizadosNoBanco(membro) {
    let enderecos = [];
    membro.enderecos.forEach(a => {
      let endereco = {
        id: { value : a.id},
        cep: { value : a.cep},
        logradouro: { value : a.logradouro},
        bairro: { value : a.bairro},
        cidade: { value : a.cidade},
        uf: { value : a.uf},
        complemento: { value : a.complemento},
      }
      enderecos.push(endereco);
    });

    let telefones = [];
    membro.telefones.forEach(a => {
      let telefone = {
        id: { value : a.id},
        tipo: { value : a.tipo},
        numero: { value : a.numero},
      }
      telefones.push(telefone);
    });

    let emails = [];
    membro.emails.forEach(a => {
      let email = {
        id: { value : a.id},
        descricao: { value : a.descricao}
      }
      emails.push(email);
    });

    this.setState({
      formControls: {
        id: {value: membro.id},
        nome: {value: membro.nome},
        cpf: {value: membro.cpf},
      },
      enderecos: enderecos,
      telefones: telefones,
      emails: emails
    });
  }

  changeHandler = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      formControls: {
        ...this.state.formControls, [name]: { ...this.state.formControls[name], value }
      }
    })
  }

  changeHandlerEndereco = event => {
    const name = event.target.name.split('.')[1];
    const value = event.target.value;
    const key = event.target.name.split('.')[0].replace("(", "").replace(")", "");
    this.state.enderecos[key][name].value = value;
    this.setState(this.state)
  }

  changeHandlerTelefone = event => {
    const name = event.target.name.split('.')[1];
    const value = event.target.value;
    const key = event.target.name.split('.')[0].replace("(", "").replace(")", "");
    this.state.telefones[key][name].value = value;
    this.setState(this.state)
  }

  changeHandlerEmails = event => {
    const name = event.target.name.split('.')[1];
    const value = event.target.value;
    const key = event.target.name.split('.')[0].replace("(", "").replace(")", "");
    this.state.emails[key][name].value = value;
    this.setState(this.state)
  }

  salvarAtualizarMembro = () => {
    let enderecos = [];
    this.state.enderecos.forEach(a => {
      let endereco = {
        id:  a.id.value,
        cep:  a.cep.value,
        logradouro: a.logradouro.value,
        bairro: a.bairro.value,
        cidade:a.cidade.value,
        uf:  a.uf.value,
        complemento: a.complemento.value,
      }
      enderecos.push(endereco);
    });

    let telefones = [];
    this.state.telefones.forEach(a => {
      let telefone = {
        id: a.id.value,
        tipo: a.tipo.value,
        numero: a.numero.value,
      }
      telefones.push(telefone);
    });

    let emails = [];
    this.state.emails.forEach(a => {
      let email = {
        id: a.id.value,
        descricao: a.descricao.value
      }
      emails.push(email);
    });

    let objeto = {
      id: this.state.formControls.id.value,
      nome:  this.state.formControls.nome.value,
      cpf: this.state.formControls.cpf.value,
      enderecos: enderecos,
      telefones: telefones,
      emails: emails,
    }

    axios.post(`${API_MEMBRO_URL}`, objeto).then(res => {
      Swal.fire('', 'Salvo com sucesso!', '', 'success');
      this.props.history.push("/dashboard");
    }, (e) => {
      Swal.fire('Erro', 'Ocorreu um erro Inesperado', '', 'error');
    });
  }

  adicionarNovoEndereco = () => {
    this.state.enderecos.push({
      id: { value : ''},
      cep: { value : ''},
      logradouro: { value : ''},
      bairro: { value : ''},
      cidade: { value : ''},
      uf: { value : ''},
      complemento: { value : ''},
    })
    this.setState(this.state)
  }

  adicionarNovoTelefone = () => {
    this.state.telefones.push({
      id: { value : '' },
      tipo: { value : '' },
      numero: { value : '' },
    })
    this.setState(this.state)
  }

  adicionarNovoEmail = () => {
    this.state.emails.push({
      id: { value : ''},
      descricao: { value : ''}
    })
    this.setState(this.state)
  }

  render() {
    return (
      <>
        <div className="wrapper">
          <div className="main">
            <Container>
              <form>
                <Row>
                  <Col lg="12" sm="12">
                    <h3>Dados Pessoais</h3>
                    <div className="form-group">
                      <label>Nome</label>
                      <input type="text"
                             name="nome"
                             className="form-control"
                             value={this.state.formControls.nome.value}
                             onChange={this.changeHandler}
                      />
                    </div>

                    <div className="form-group">
                      <label>CPF</label>
                      <InputMask mask="999.999.999-99" name="cpf" className="form-control" value={this.state.formControls.cpf.value} onChange={this.changeHandler}/>
                    </div>
                  </Col>
                </Row>

                <hr />

                <Row>
                    <Col lg="12" sm="12">
                        <h3>Enderešos</h3>
                        <Row>
                          <Col lg="12" sm="12">
                            <button type="button" onClick={this.adicionarNovoEndereco} className="pull-right btn btn-success"><i class={"fa fa-plus"}></i></button>
                          </Col>
                        </Row>
                        {
                          this.state.enderecos.map((a, k) => {
                            return (
                                <div className="d-flex flex-row">
                                  <div>
                                    <label>Cep</label>
                                    <div className="input-group mb-3">
                                      <input type="text" name={'(' + k + ').cep'} className="form-control" value={a.cep.value} onChange={this.changeHandlerEndereco}/>
                                      <div className="input-group-append" style={maginTop}>
                                        <button type="button" onClick={this.carregaDadosViaCep(a.cep.value, k)} className="btn btn-primary">Buscar</button>
                                      </div>
                                    </div>
                                  </div>

                                  <div>
                                    <label>Logradouro</label>
                                    <input type="text" name={'(' + k + ').logradouro'} className="form-control"value={a.logradouro.value} onChange={this.changeHandlerEndereco}/>
                                  </div>

                                  <div lg="2" className="form-group">
                                    <label>Bairro</label>
                                    <input type="text" name={'(' + k + ').bairro'} className="form-control" value={a.bairro.value} onChange={this.changeHandlerEndereco}/>
                                  </div>

                                  <div>
                                    <label>Cidade</label>
                                    <input type="text" name={'(' + k + ').cidade'} className="form-control"  value={a.cidade.value} onChange={this.changeHandlerEndereco}/>
                                  </div>

                                  <div>
                                    <label>Uf</label>
                                    <input type="text" name={'(' + k + ').uf'} className="form-control" value={a.uf.value} onChange={this.changeHandlerEndereco}/>
                                  </div>

                                  <div lg="2" className="form-group">
                                    <label>Complemento</label>
                                    <input type="text" name={'(' + k + ').complemento'} className="form-control"  value={a.complemento.value} onChange={this.changeHandlerEndereco}/>
                                  </div>
                                </div>
                            )
                          })
                        }
                    </Col>
                </Row>

                <hr />


                <Row>
                    <Col lg="12" sm="12">
                      <h3>Telefone</h3>

                      <Row>
                        <Col lg="12" sm="12">
                          <button type="button" onClick={this.adicionarNovoTelefone} className="pull-right btn btn-success"><i class={"fa fa-plus"}></i></button>
                        </Col>
                      </Row>

                      <table className="table">
                        <thead>
                        <tr>
                          <th>Tipo</th>
                          <th>Numero</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                          this.state.telefones.map((a, k) => {
                            return (
                                <tr>
                                  <th><input type="text" name={'(' + k + ').tipo'} className="form-control" value={a.tipo.value} onChange={this.changeHandlerTelefone}/></th>
                                  <th>
                                    <InputMask mask="(99) 9 9999-9999" name={'(' + k + ').numero'} className="form-control" value={a.numero.value} onChange={this.changeHandlerTelefone}/>
                                  </th>
                                </tr>
                            )
                          })
                        }
                        </tbody>
                      </table>
                    </Col>

                    <hr />

                    <Col lg="12" sm="12">
                      <h3>Emails</h3>

                      <Row>
                        <Col lg="12" sm="12">
                          <button type="button" onClick={this.adicionarNovoEmail} className="pull-right btn btn-success"><i class={"fa fa-plus"}></i></button>
                        </Col>
                      </Row>

                      <table className="table">
                        <thead>
                        <tr>
                          <th>descricao</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                          this.state.emails.map((a, k) => {
                            return (
                                <tr>
                                  <th><input type="text" name={'(' + k + ').descricao'} className="form-control" value={a.descricao.value} onChange={this.changeHandlerEmails}/></th>
                                </tr>
                            )
                          })
                        }
                        </tbody>
                      </table>
                    </Col>

                    <button type="button" onClick={this.salvarAtualizarMembro} className="btn btn-primary">Salvar</button>
                    <Link to={'/dashboard'} type="button" className="btn btn-default">Voltar</Link>
                </Row>
              </form>
            </Container>
          </div>
        </div>
      </>
    );
  }
}

export default UserDetails;
