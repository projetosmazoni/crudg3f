import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import "assets/css/nucleo-icons.css";
import "assets/scss/blk-design-system-react.scss?v=1.0.0";
import "assets/demo/demo.css";

import Index from "views/Index.jsx";
import UserDetails from "./components/user-details/UserDetails";
import EventDetails from "./components/event/EventDetails";
import Basics from "./views/IndexSections/Basics";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/components" render={props => <Index {...props} />} />
      <Route path="/user-details/:id" render={props => <UserDetails {...props} />} />
      <Route path="/new-user" render={props => <UserDetails {...props} />} />
      <Route path="/new-event" render={props => <EventDetails {...props} />} />
      <Route path="/dashboard" render={props => <Basics {...props} />} />
      <Redirect from="/" to="/components" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
