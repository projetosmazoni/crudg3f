package com.br.clisys.controller;

import com.br.clisys.entity.UserLogin;
import com.br.clisys.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/api/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping
    public ResponseEntity<?> salvar(@Valid @RequestBody UserLogin usuario) {
        try {
            UserLogin user = this.loginService.autentica(usuario);

            if (user == null) {
                return new ResponseEntity<>("Login Invalido", HttpStatus.UNAUTHORIZED);
            }

            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Login Invalido", HttpStatus.UNAUTHORIZED);
        }
    }
}
