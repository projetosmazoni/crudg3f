package com.br.clisys.controller;

import com.br.clisys.exceptions.BadRequestException;
import com.br.clisys.service.GenericService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public abstract class GenericRestController<T, ID> {

    private Logger logger = LoggerFactory.getLogger(GenericRestController.class);

    private GenericService<T, ID> service;

    public GenericRestController(GenericService<T, ID> service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<?>> buscarTodos() {
        return new ResponseEntity<>(this.service.buscarTodos(), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> buscaPorId(@PathVariable(name = "id") ID id) {
        T entity = this.service.buscaPorId(id);
        return  isNull(entity) ? ResponseEntity.notFound().build() : ResponseEntity.ok(entity);
    }

    @PostMapping
    public ResponseEntity<?> salvar(@Valid @RequestBody T entity, BindingResult result) {
        try {
            if (result.hasErrors()) {
                String error = result.getAllErrors().stream().map(a -> a.getDefaultMessage()).collect(Collectors.joining(","));
                return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
            }

            return new ResponseEntity<>(this.service.salvar(entity), HttpStatus.OK);
        } catch (Exception e) {
            if (e instanceof BadRequestException) {
                return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> remove(@PathVariable(name = "id") ID id) {
        T entity = this.service.buscaPorId(id);

        if (isNull(entity)) {
            return ResponseEntity.notFound().build();
        }

        this.service.deletar(id);
        return ResponseEntity.noContent().build();
    }

}
