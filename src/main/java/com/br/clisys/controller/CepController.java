package com.br.clisys.controller;

import com.br.clisys.entity.UserLogin;
import com.br.clisys.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.text.MessageFormat;

@Controller
@RequestMapping("/api/cep")
public class CepController {

    @GetMapping(path = "/{cep}")
    public ResponseEntity<?> salvar(@PathVariable String cep) {
        return new RestTemplate().getForEntity("https://viacep.com.br/ws/" + cep + "/json", Object.class);
    }
}
