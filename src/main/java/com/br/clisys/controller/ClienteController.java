package com.br.clisys.controller;

import com.br.clisys.entity.Cliente;
import com.br.clisys.service.GenericService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController extends GenericRestController<Cliente, Long> {

    public ClienteController(GenericService<Cliente, Long> service) {
        super(service);
    }


}
