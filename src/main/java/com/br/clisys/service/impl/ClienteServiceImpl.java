package com.br.clisys.service.impl;

import com.br.clisys.entity.Cliente;
import com.br.clisys.exceptions.BadRequestException;
import com.br.clisys.repository.ClienteRepository;
import com.br.clisys.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl extends GenericServiceImpl<Cliente, Long> implements GenericService<Cliente, Long> {

    @Autowired
    private ClienteRepository repository;

    @Override
    public Cliente salvar(Cliente entity) throws BadRequestException {
        validaDadosRecebidos(entity);
        return repository.save(entity);
    }

    private void validaDadosRecebidos(Cliente entity) throws BadRequestException {
        if(entity.getEnderecos().isEmpty()) {
            throw new BadRequestException("É necessario cadastrar pelo menos um endereço");
        }

        if(entity.getTelefones().isEmpty()) {
            throw new BadRequestException("É necessario cadastrar pelo menos um Telefone");
        }

        if(entity.getEmails().isEmpty()) {
            throw new BadRequestException("É necessario cadastrar pelo menos um E-mail");
        }
    }
}
