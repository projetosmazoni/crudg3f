package com.br.clisys.service.impl;

import com.br.clisys.entity.Cliente;
import com.br.clisys.entity.UserLogin;
import com.br.clisys.exceptions.BadRequestException;
import com.br.clisys.repository.ClienteRepository;
import com.br.clisys.repository.UserLoginRepository;
import com.br.clisys.service.GenericService;
import com.br.clisys.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserLoginRepository repository;

    @Override
    public UserLogin autentica(UserLogin nome) {
        return repository.findByLoginAndSenha(nome.getLogin(), nome.getSenha());
    }
}
