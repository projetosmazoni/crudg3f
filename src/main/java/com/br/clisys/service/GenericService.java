package com.br.clisys.service;

import com.br.clisys.exceptions.BadRequestException;

import java.util.List;

public interface GenericService<T, Key> {
    List<T> buscarTodos();
    T buscaPorId(Key id);
    T salvar(T entity) throws BadRequestException;
    void deletar(Key id);
}
