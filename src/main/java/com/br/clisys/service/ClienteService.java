package com.br.clisys.service;

import com.br.clisys.entity.Cliente;

public interface ClienteService extends GenericService<Cliente, Long> {

}
