package com.br.clisys.exceptions;

public class BadRequestException extends Exception {
    private static final long serialVersionUID = 1149241039409861914L;

    public BadRequestException(String msg){
        super(msg);
    }

    public BadRequestException(String msg, Throwable cause){
        super(msg, cause);
    }
}
