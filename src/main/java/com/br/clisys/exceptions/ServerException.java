package com.br.clisys.exceptions;

public class ServerException extends Exception {
    private static final long serialVersionUID = 1149241039409861914L;

    public ServerException(String msg){
        super(msg);
    }

    public ServerException(String msg, Throwable cause){
        super(msg, cause);
    }
}
