package com.br.clisys.repository;

import com.br.clisys.entity.UserLogin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLoginRepository extends CrudRepository<UserLogin, Long> {

    UserLogin findByLoginAndSenha( String login, String senha);
}
