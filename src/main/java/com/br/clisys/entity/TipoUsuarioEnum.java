package com.br.clisys.entity;

public enum TipoUsuarioEnum {

	ADMIN("ADMIN"),
	COMUM("COMUM");

	public String descricao;

	TipoUsuarioEnum(String descricao){
		this.descricao = descricao;
	}

}
