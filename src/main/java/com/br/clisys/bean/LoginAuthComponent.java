package com.br.clisys.bean;

import com.br.clisys.entity.TipoUsuarioEnum;
import com.br.clisys.entity.UserLogin;
import com.br.clisys.repository.UserLoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Component
public class LoginAuthComponent {

    @Autowired
    private Environment environment;

    @Autowired
    private UserLoginRepository userLoginRepository;

    @PostConstruct
    public void init() {
        UserLogin login = new UserLogin();
        login.setLogin("admin");
        login.setSenha("123456");
        login.setTipoUsuario(TipoUsuarioEnum.ADMIN);

        UserLogin login2 = new UserLogin();
        login2.setLogin("comum");
        login2.setSenha("123456");
        login2.setTipoUsuario(TipoUsuarioEnum.COMUM);
        userLoginRepository.saveAll(Arrays.asList(login, login2));
    }

}
